# Размеченные данные для задачи NER QA

Данные (500 статей тематики мошеничество (fin crime) с РБК из архива RBC_corpus_for_annotations.zip) размечены с помощью он-лайн утилиты lighttag.io

Результат разметки утилиты находится в архиве lighttag_annotations.zip 

Скрипт для преобразования размеченных файлов утилитой lighttag.io в данные стандарта NER - lighttag.py

Lighttag.py ждет на входе файлы вида Lighttag*.csv и создает один файл data_for_deep_ner500.csv - результат преобразования.

lighttag_mix.csv - файл для обучения модели NER QA в библиотеке kashgari - содержит те же данные, что и data_for_deep_ner500.csv, но в формате pandas со стоблцами tags, texts
