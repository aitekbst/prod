# -*- coding: utf-8 -*-
"""
Created on Tue Apr 23 17:02:38 2019

@author: Илья
"""

### склеить все файлы от lightag в один

import re
import pandas as pd
import glob
files = glob.glob(r'c:\WinPython64\notebooks\Lighttag*.csv')

full = pd.DataFrame()

for k in range(len(files)):
    df1 = pd.read_csv(files[k])
    print(len(df1))
    if k==3:
        df1 = df1.drop([49,48,98]).reset_index()

    list_of_tags = []
    for i in range(0, len(df1['tags'])):
        list_of_tags.append(df1['tags'][i].split(' '))
   
    list_of_texts = []
    for i in range(0, len(df1['texts'])):
        ### ИСКАТЬ  в тексте /n и выкидывать нахуй!!!        
        list_of_texts.append(df1['texts'][i].replace('\n','').replace('\r','').split(' '))
        
    # определим начало/конец каждого слово и поставим соотв. теги.
    row = 0
    named_entity_labels_for_training = []
    texts_for_training = []
    # сопоставили слово и его начало/конец
    for row in range(len(df1)):
        d = {}
        temp = 0
                        # почистим код, убрав название новости. Убрав весь текст после запятой, если после нее нет пробела.
        string = re.sub(r",\S.*",'',df1.texts[row].replace('\n','').replace('\r',''))
        for word in string.split(' '):
            if word!='':
                #print(word,df1.texts[row].index(word), df1.texts[row].index(word)+len(word)-1)
                if list_of_tags[row][temp]!='O':
                    #print(list_of_tags[row][temp],string.index(word), string.index(word)+len(word)-1 )
                    # if key not in dict - make it, otherwise append
                    if list_of_tags[row][temp] not in d:
                        d[list_of_tags[row][temp]]=[(string.index(word), string.index(word)+len(word))]
                    else:
                        # if value already there, do not append
                        if (string.index(word), string.index(word)+len(word)) not in d[list_of_tags[row][temp]]:
                            d[list_of_tags[row][temp]].append((string.index(word), string.index(word)+len(word)))
                temp+=1
        named_entity_labels_for_training.append(d)
        texts_for_training.append(string)
        
    full = pd.concat([full, pd.DataFrame(list(map(list, zip(texts_for_training,named_entity_labels_for_training))), columns=['texts','labels'])])
full.reset_index(drop=True, inplace=True)
full.to_csv('data_for_deep_ner500.csv', index = False)