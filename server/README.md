Алгоритм работы с разработанной системой
Запуск сервиса на сервере AITEK (VPN: @bst-nlp.csi-group.local):
systemctl start flask

Посмотреть логи можно командой:
journalctl -u flask -r

Классификатор ждет команды по адресу:	 	http://127.0.0.1:5000/classifier

NER по адресу: 					http://127.0.0.1:5000/ner

NER QA по адресу: 					http://127.0.0.1:5000/ner_qa


Пример команд curl:

*классификатор*

curl --header "Content-Type: application/json" \
 --request POST \
 --data '{"text":"С 1 января 2016 года в Северной столице выросли цены на", "entity_names":"[января, года]","title":"Заголовок" }' http://127.0.0.1:5000/classifier

Все поля (text, entity_names, title) - обязательные. При подаче запроса через curl в тексте не должно быть переносов строк.

Если в entity_names нет слова, находящегося в text, то ответ: Irrelevant

Если в entity_names есть слова, находящегося в text, то включается классификатор по text и выдает ответ: Fin Crime или Neutral (если определенный класс текста не равен ‘Fin Crime’)

Если нет полей или формат полей не верный, выдается сообщение типа: 

Status code 400

{'Error': 'Missing Field: text', 'success': False}

Пример ответа, когда в запросе нет ошибок

status code: 200

{"class":"Neutral","entity_names":["[января"," года]"],"success":true

*NER (выделение сущностей)*

curl --header "Content-Type: application/json" --request POST  --data '{"text":"С 1 января 2016 года в Северной столице выросли цены на" }' http://127.0.0.1:5000/ner

Поле (text) - обязательное. При подаче запроса через curl в тексте не должно быть переносов строк.

*NER QA (Ответы на вопросы по новости)*

curl --header "Content-Type: application/json" --request POST  --data '{"text":"Воронежские полицейские возбудили уголовное дело в отношении руководителя воронежского филиала одного из федеральных госпредприятий . Об этом сообщила пресс-служба ГУ МВД по Воронежской области без указания имени подозреваемого . По сведениям РБК-Черноземье , речь идет о бывшем руководителе воронежского филиала Почта России Юрии Бесхмельницыне."}'  http://127.0.0.1:5000/ner_qa

Поле (text) - обязательное. При подаче запроса через curl в тексте не должно быть переносов строк.
