# -*- coding: utf-8 -*-
"""
Created on Wed Apr 10 12:28:46 2019

@author: Илья
"""
import pandas as pd
#import deeppavlov 
import fastText
import pymorphy2
import argparse
import os
from deeppavlov import build_model, configs

ner_model = build_model(configs.ner.ner_rus , download=True)
