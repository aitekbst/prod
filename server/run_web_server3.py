# -*- coding: utf-8 -*-
"""
With deeppavlov!

Code for web server on flask
requests by curl with ru simbols give errors.
requests by python or directly by http is ok.

@author: IKuznetsov
"""

import flask
import tensorflow as tf
from helpers import *
from kashgari.tasks.classification import BLSTMModel
from kashgari.tasks.seq_labeling import BLSTMCRFModel
import pymorphy2
import NER_lib
import pickle

# instantiate flask 
app = flask.Flask(__name__)

# load the model
global graph
graph = tf.get_default_graph()

model = BLSTMModel.load_model('/opt/aitek/classifier/model_wo_emb/')

# NER QA with kashgari
model_ner_qa = BLSTMCRFModel.load_model('/opt/aitek/classifier/BLSTMCRF_NER_Kashgari_wo_emb')

def dict_to_words_list(text, dictionary):
    d3 = dict()
    for i in dictionary.keys():
        a = []
        for j in dictionary[i]:
            a.append(text[j[0]:j[1]])
        d3[i] = a
    return d3


# expected Fields for Classification
expectedFields = ['entity_names','title','text']
# expected Fields for NER
expectedFields_ner = ['text']
expectedFields_question = ['text']


@app.route("/",  methods=["POST"])
def homepage():
	return "Welcome to the API for rus BS NLP. <br> Use <b>/predict</b> to run classification\
	<br> Also use requests directly to server"

# define a predict function as an endpoint 
@app.route("/classifier", methods=["POST"])
def predict():
    data = {"success": False}
    params = flask.request.json
    if (params == None):
        params = flask.request.args
    missing = checkJson(expectedFields, params)
    if missing:
        # and send 400, Bad Request
        #print(missing)
        data['Error'] = missing
        return flask.jsonify(data), 400
    else:
        # if parameters are found and they correct, return a prediction
        data["entity_names"] = params.get("entity_names") # entity_names через запятую, парсим их
        data["entity_names"] = data["entity_names"].split(sep=',')
        # прогоняем NER и находим entity_names в тексте, сравниваем их  #### пока делаем вручную: токенизация и лемматизацию
        # для этого делаем лемматизацию - приведение слова к ед. числу, им. падеж
        # quick and dirty -- пока работает если entity_names  состоят из одного слова. Потом будем брать entities из скрипта NER Карины.
        # lemmatization
        entity_names = [preprocess_text(x) for x in data["entity_names"]]
        text = params.get("text")
        lemmas = mystem.lemmatize(text)
        flag = 0
        for entity in entity_names:
            if entity in lemmas:
                flag = 1
        # if there is no entities in text then it is irrelevant
        if flag==0:
            data['class'] = 'Irrelevant'
        # if there is entities - than we make prediction and based on prediction return Fin Crime or Neutral
        else:
            text = nltk.word_tokenize(text)
            with graph.as_default():
                prediction = model.predict(text)

            if prediction == 'Fin Crime':
                data['class'] = 'Fin Crime'
            else:
                data['class'] = 'Neutral'
        data["success"] = True
    # return a response in json format 
    return flask.jsonify(data)    

@app.route("/ner", methods=["POST"])
def ner():
    data = {"success": False}

    params = flask.request.json
    if (params == None):
        params = flask.request.args
    
    missing = checkJson(expectedFields_ner, params)
    if missing:
        data['Error'] = missing
        return flask.jsonify(data), 400

    else:
        # if parameters are found and they correct, return a prediction
        data["text"] = params.get("text")
        res = NER_lib.ner_model([data["text"]])
        tags = []
        words = []
    
        for i in range(0, len(res[1][0])):
            if res[1][0][i]!='O':
                tags.append(res[1][0][i])
                words.append(res[0][0][i])

        the_res = [tags, words]

        morph = pymorphy2.MorphAnalyzer()

        pers = []
        orgs = []
        locs = []
        for j in range(0, len(the_res[0])):
            if the_res[0][j]=='B-PER' or the_res[0][j]=='I-PER':
                text = the_res[1][j]
                pers.append(morph.parse(text)[0].normal_form)
            if the_res[0][j]=='B-ORG' or the_res[0][j]=='I-ORG':
                text = the_res[1][j]
                orgs.append(morph.parse(text)[0].normal_form)
            if the_res[0][j]=='B-LOC' or the_res[0][j]=='I-LOC':
                text = the_res[1][j]
                locs.append(morph.parse(text)[0].normal_form)
        
        data['Persons'] = pers
        data['Organizations'] = orgs
        data['Locations'] = locs

        data["success"] = True

    # return a response in json format 
    return flask.jsonify(data)    
    

@app.route("/ner_qa", methods=["POST"])
def ner_qa():
    data = {"success": False}
    params = flask.request.json
    if (params == None):
        params = flask.request.args
    missing = checkJson(expectedFields_ner, params)
    if missing:
        data['Error'] = missing
        return flask.jsonify(data), 400
    else:
        # if parameters are found and they correct, return a prediction
        data["text"] = params.get("text")
        text = data["text"].split(' ')
        with graph.as_default():
            result = model_ner_qa.predict(text)
        OFF, LOC, DAT, SUF = [],[],[],[]
        for i in range(len(result)):
            if result[i]=='OFF':
                OFF.append(text[i])
            if result[i]=='LOC':
                LOC.append(text[i])
            if result[i]=='DAT':
                DAT.append(text[i])
            if result[i]=='SUF':
                SUF.append(text[i])
        OFF = list(dict.fromkeys(OFF))
        LOC = list(dict.fromkeys(LOC))
        DAT = list(dict.fromkeys(DAT))
        SUF = list(dict.fromkeys(SUF))
        # make dict from this bezobrazie
        result = {'OFF':OFF,'LOC':LOC,'DAT':DAT,'SUF':SUF}
        for key in ['LOC','OFF','DAT','SUF']:
            if key in result:
                data[key] = result[key]
        data["success"] = True
    # return a response in json format 
    return flask.jsonify(data)    




if __name__ == "__main__":
    print("* Starting web service...")
    app.config['JSON_AS_ASCII'] = False
    app.run()
    