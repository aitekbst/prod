# -*- coding: utf-8 -*-
"""
helpers functions

@author: Илья

Error Messages  In case of an error a field named ‘message’ provides one of the following
messages:
● Missing Field [Parameter]
● Field [Parameter] should be of type [Parameter]
● Service Error
HTTP Codes  
● 200 Success
● 400 Bad Request

"""
import nltk
nltk.download("stopwords")
nltk.download("punkt")

from nltk.corpus import stopwords
from pymystem3 import Mystem
from string import punctuation

#Create lemmatizer and stopwords list
mystem = Mystem() 
russian_stopwords = stopwords.words("russian")

def checktype(obj):
    try:
        return bool(obj) and all(isinstance(elem, str) for elem in obj)
    except (RuntimeError, TypeError, NameError):
        return False


def checkJson(expectedFields,data):
    if not data:
        return 'data'
    
    # Missing Field [Parameter]
    for f in expectedFields:
        try:
            data[f]
        except:
            return 'Missing Field: '+ str(f)

    # entity_names  ['','','','','']
    try:
        if checktype(data['entity_names'])==False:
            return 'Field [entity_names] should be of type List of strings'
    except KeyError:
       pass 
        
    # title
    try:
        if isinstance(data['title'], str)==False:
            return 'Field [title] should be of type String'
    except KeyError:
       pass
       
    # text
    try:
        if isinstance(data['text'], str)==False:
            return 'Field [text] should be of type String'
    except KeyError:
       pass
       
def preprocess_text(text):
    tokens = mystem.lemmatize(text.lower())
    tokens = [token for token in tokens if token not in russian_stopwords\
              and token != " " \
              and token.strip() not in punctuation]
    
    text = " ".join(tokens)
    
    return text